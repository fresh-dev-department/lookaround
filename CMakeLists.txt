cmake_minimum_required(VERSION 3.10)
project(lookaround C)

set(CMAKE_C_STANDARD 11)

include_directories(.)
include_directories(src)
include_directories(src/core)
include_directories(src/core/managers)
include_directories(src/export)
include_directories(src/lib)
include_directories(src/lib/network)
include_directories(src/log)
include_directories(src/modules)

add_executable(lookaround
        src/core/managers/arguments.c
        src/core/managers/arguments.h
        src/core/managers/export.c
        src/core/managers/export.h
        src/core/managers/modules.c
        src/core/managers/modules.h
        src/core/core.c
        src/core/core.h
        src/core/module.c
        src/core/module.h
        src/core/register.h
        src/lib/network/ip_addr_validator.c
        src/lib/network/ip_addr_validator.h
        src/modules/open_ports_syn_scanner.c
        src/modules/open_ports_syn_scanner.h
        src/modules/vulners_search.c
        src/modules/vulners_search.h
        src/main.c
        LICENSE.md
        README.md)
