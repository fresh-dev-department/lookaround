#include "ip_addr_validator.h"

#include <ctype.h>
#include <string.h>

int is_valid_ipv4_addr (char const *ip_addr)
{
    // parsing ipv4 without any libs, just for fun
    // 0-255.0-255.0-255.0-255 is a valid ip_addr.

    int segments = 0;

    int current_number = 0;
    int segment_number = 0;
    int segment_number_period = 100;

    if (ip_addr == NULL)
        return 0;

    while (*ip_addr != '\0') {
        if (isdigit (*ip_addr)) {
            current_number = *ip_addr - '0';
            
            if (0 > current_number || current_number > 9)
                return 0;

            if (0 > segment_number || segment_number > 255)
                return 0;

            if (segment_number_period == 0)
                return 0;

            segment_number = segment_number + segment_number_period * current_number;
            segment_number_period /= 10;
        } else if (*ip_addr == '.') {
            if (segments >= 3)
                return 0;

            segments++;
            segment_number = 0;
            segment_number_period = 100;
        } else {
            return 0;
        }

        ip_addr++;
    }

    return (segments == 3) ? 1 : 0;
}

int is_valid_ipv4_addrs (int addrs_count, char const *ip_addrs[])
{
    if (*ip_addrs == NULL || addrs_count <= 0)
        return 0;

    for (int i = 0; i < addrs_count; i++) {
        if (!is_valid_ipv4_addr(ip_addrs[i]))
            return 0;
    }

    return 1;
}