#ifndef _IP_ADDR_VALIDATOR_
#define _IP_ADDR_VALIDATOR_

/*
    Validates if |ip| is exactly as in the IPv4 standard.
    Returns 1 if ip is correct, 0 if it's not.
*/
int is_valid_ipv4_addr (char const *ip_addr);

/*
    Validates range of |ips| using validate_ip().
    Returns 1 if all ips are ok, 0 if they're not.
*/
int is_valid_ipv4_addrs (int addrs_count, char const *ip_addrs[]);

#endif // _IP_ADDR_VALIDATOR_