#include "module.h"

#include <stdlib.h>
#include <string.h>

module_data_t *create_module_data (module_data_type_t type, void *value)
{
    module_data_t *module_data = malloc (sizeof (module_data_t));
    if (!module_data)
        return NULL;

    module_data->type = type;
    module_data->value = value;

    return module_data;
}

module_error_t *create_module_error (module_error_type_t type, char *text)
{
    module_error_t *module_error = malloc (sizeof (module_error_t));
    if (!module_error)
        return NULL;

    module_error->type = type;
    module_error->text = strdup (text);

    return module_error;
}

void free_module_data (module_data_t *module_data)
{
    if (!module_data)
        return;

    free (module_data);
    module_data = NULL;
}

void free_module_error (module_error_t *module_error)
{
    if (!module_error)
        return;

    free (module_error->text);
    free (module_error);

    module_error = NULL;
}

module_result_t *create_module_result (module_data_t *data, 
    module_error_t *error)
{
    module_result_t *result = malloc (sizeof (module_result_t));
    if (!result)
        return NULL;

    result->data = data;
    result->error = error;

    return result;
}

void free_module_result (module_result_t *result)
{
    if (!result)
        return;

    free (result->data);
    result->data = NULL;

    free (result->error);
    result->error = NULL;

    free (result);
    result = NULL;
}

module_type_t get_module_type (const module_t *module)
{
    if (!module)
        return MOD_NULL;

    return module->_type;
}

const char *get_module_name (const module_t *module)
{
    if (!module)
        return "";

    return module->_name;
}

int get_module_in_data_type_count (const module_t *module)
{
    if (!module)
        return 0;

    return module->_in_data_type_count;
}

int get_module_out_data_type_count (const module_t *module)
{
    if (!module)
        return 0;

    return module->_out_data_type_count;
}

static module_data_type_t _err_data_types[] = {MOD_DATA_ERR};

module_data_type_t *get_module_in_data_types (const module_t *module)
{
    if (!module)
        return _err_data_types;

    return module->_in_data_types;
}

module_data_type_t *get_module_out_data_types (const module_t *module)
{
    if (!module)
        return _err_data_types;

    return module->_out_data_types;
}