#ifndef _CORE_
#define _CORE_

#include "managers/export.h"
#include "managers/modules.h"
#include "managers/arguments.h"

/*
*/
typedef enum core_settings {
    CORE_SET_SCANONLY,
    CORE_SET_DETECTONLY,
    CORE_SET_SEARCHONLY,
    CORE_SET_BRUTEFORCEONLY,
    CORE_SET_NOSCAN,
    CORE_SET_NODETECT,
    CORE_SET_NOSEARCH,
    CORE_SET_NOBRUTEFORCE
} core_settings_t;

/*
*/
typedef struct core {
    char *appname;
    core_settings_t core_settings;
} core_t;

/*
*/
core_t *create_core (const char *appname, core_settings_t core_settings);
void free_core (core_t *core);

/*
*/
int run (core_t *self);

#endif