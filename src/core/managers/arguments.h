#ifndef _ARGUMENTS_MANAGER_
#define _ARGUMENTS_MANAGER_

/*
    All posible arguments are defined there.
*/
typedef enum argument_type {
    ARG_HELP,
    ARG_DEBUG,
    ARG_VERSION,
    
    ARG_SCANONLY,
    ARG_DETECTONLY,
    ARG_SEARCHONLY,
    ARG_BRUTEFORCEONLY,

    ARG_NOSCAN,
    ARG_NODETECT,
    ARG_NOSEARCH,
    ARG_NOBRUTEFORCE,

    ARG_NETADDR,
    ARG_IPADDR,
    ARG_PORT,

    ARG_EXPORTXML,
    ARG_EXPORTJSON,
    ARG_EXPORTPLAINTEXT,

    ARG_INVALID
} argument_type_t;

/*
*/
typedef struct argument {
    argument_type_t type;
    char *value;
} argument_t;

argument_t *parse_arguments (int argc, const char *argv[]);

#endif