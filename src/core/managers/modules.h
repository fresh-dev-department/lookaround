#ifndef _MODULES_MANAGER_
#define _MODULES_MANAGER_

#include "../module.h"

/*
*/
typedef struct payload {
    module_t *module;
    int argc;
    module_data_t *argv;
} payload_t;

/*
    An array of all registered modules;
*/
extern module_t *registered_modules[];

/*
    Checks if requested |data_types| coincide with |next_data_types| from
    the next module.

    Returns 1 if true, else 0.
*/
static int interests_coincide (
    int data_type_count, module_data_type_t data_types[],
    int next_data_type_count, module_data_type_t next_data_types[]);

/*
    Searching for next modules by their input data types using output data types
    from module executing result.

    Uses linear search because of little amount of modules.
*/
module_t **get_next_modules (int data_type_count, 
    module_data_type_t data_types[]);

/*
    Running |modules| with amount of threads set in |thread_count|.
*/
module_result_t *run_modules (const payload_t *playloads[], int thread_count);

/*
    Debug function.
*/
void test_modules (void);


#endif
