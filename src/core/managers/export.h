#ifndef _EXPORT_MANAGER_
#define _EXPORT_MANAGER_

typedef enum export_settings {
    EXPORT_SET_XML,
    EXPORT_SET_JSON,
    EXPORT_SET_PLAINTEXT
} export_settings_t;

#endif