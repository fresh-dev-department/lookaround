#include "modules.h"

#include "../register.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int interests_coincide (int data_type_count, module_data_type_t data_types[],
    int next_data_type_count, module_data_type_t next_data_types[])
{
    if (next_data_type_count > data_type_count)
        return 0;

    int coincided_interest_count = 0;

    for (int i = 0; i < data_type_count; i++) {
        for (int j = 0; j < next_data_type_count; j++) {
            if (data_types[i] == next_data_types[j]) {
                coincided_interest_count++;
                break;
            }
        }
    }

    return (coincided_interest_count == next_data_type_count) ? 1 : 0;
}

module_t **get_next_modules (int data_type_count, 
    module_data_type_t data_types[])
{
    int next_module_count = 0;

    module_t **next_modules = malloc (sizeof (module_t*));
    if (!next_modules)
        return NULL;

    int prev_data_types_size = 0;

    module_t **module;  
    for (module = registered_modules; *module != NULL; module++) {
        int next_data_type_count = get_module_in_data_type_count (*module);
        module_data_type_t *next_data_types = get_module_in_data_types (*module);

        if (interests_coincide (data_type_count, data_types,
                                next_data_type_count, next_data_types)) {
            next_modules[next_module_count] = *module;

            next_modules = realloc (next_modules, 
                (++next_module_count + 1) * sizeof (module_t *));
            if (!next_modules)
                return NULL;
        }
    }

    next_modules[next_module_count] = NULL;
    return next_modules;
}

module_result_t *run_modules (const payload_t *playloads[], int thread_count) 
{

}

void test_modules (void)
{
    module_t **module;
    for (module = registered_modules; *module != NULL; module++) {
        printf("%s\n", get_module_name(*module));
    }
}
