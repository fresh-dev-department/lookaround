#ifndef _MODULE_
#define _MODULE_

/*
    This is which in C++ might be called "module interface".
    There are structs and enums that make up module API.
    Use this file as a header file in any module in this program.
*/

typedef enum module_type {
    /* Module for scanning devices ports, website's vulns, etc. */
    MOD_SCAN,
    /* Module for detecting name, description, version, etc. */
    MOD_DETECT,
    /* Module for searching in the dbs, websites, rerurns final data. */
    MOD_SEARCH,
    /* Module for bruteforcing passwords, dirs, etc. */
    MOD_BRUTEFORCE,
    /* Replacement for NULL.*/
    MOD_NULL
} module_type_t;

typedef enum module_data_type {
    /* Final data, cannot be reused any more. Should be logged*/
    MOD_DATA_FIN,
    /* IP address data. */
    MOD_DATA_IPADDR,
    /* Port data. */
    MOD_DATA_PORT,
    /* Network address data. */
    MOD_DATA_NETADDR,
    /* Application name. */
    MOD_DATA_APPNAME,
    /* Application version. */
    MOD_DATA_APPVERSION,
    /* Other result that isn't appeared to be proceeded.*/
    MOD_DATA_OTHER,
    /* If error happend. Should be set in any out data type of module*/
    MOD_DATA_ERR
} module_data_type_t;

typedef enum module_error_type {
    /* Error in module running. */
    MOD_ERR_RUN,
    /* Error in module executing. */
    MOD_ERR_EXEC,
    /* Invalid args passed. */
    MOD_ERR_ARGS,
    /* Other error, see in error text. */
    MOD_ERR_OTHER,
    /* No error happend. */
    MOD_ERR_NULL
} module_error_type_t;

typedef struct module_data {
    module_data_type_t type;
    void *value;
} module_data_t;

typedef struct module_error {
    module_error_type_t type;
    char *text;
} module_error_t;

typedef struct module_result {
    module_data_t *data;
    module_error_t *error;
} module_result_t;

/*
    Module execution function should get an array of module_data_t 
    which is ended with NULL value. Returns an array of module_result_t
    ended with NULL value.
*/
typedef module_result_t *(*module_exec_function) (int argc, const module_data_t argv[]);

/*
    Module struct.
    |_type| 
*/
typedef struct module {
    module_type_t _type;
    
    char *_name;

    int _in_data_type_count;
    module_data_type_t *_in_data_types;

    int _out_data_type_count;
    module_data_type_t *_out_data_types;

    module_exec_function exec;
} module_t;

/*
*/
module_data_t *create_module_data (module_data_type_t type, void *value);
module_error_t *create_module_error (module_error_type_t type, char *text);

void free_module_data (module_data_t *module_data);
void free_module_error (module_error_t *module_error);

/*
*/
module_result_t *create_module_result (module_data_t *data, 
    module_error_t *error);
void free_module_result (module_result_t *result);

/*
    There are getters for the incapsulated fields in the module structure.
*/
module_type_t get_module_type (const module_t *module);

const char *get_module_name (const module_t *module);

int get_module_in_data_type_count (const module_t *module);
int get_module_out_data_type_count (const module_t *module);

module_data_type_t *get_module_in_data_types (const module_t *module);
module_data_type_t *get_module_out_data_types (const module_t *module);

#endif