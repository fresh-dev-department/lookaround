#ifndef _MODULES_
#define _MODULES_

#include "module.h"

#include <stddef.h>

extern module_t vulners_search;
extern module_t open_ports_syn_scanner;

/*
    An array of registered modules that allows use them easily.
    Should be always ended with NULL value.
*/
module_t *registered_modules[] = {
    &vulners_search, &open_ports_syn_scanner, NULL
};

#endif
