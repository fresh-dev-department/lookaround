#include "core.h"

#include <stdlib.h>
#include <string.h>

#include <stdio.h>

core_t *create_core (const char *appname, core_settings_t core_settings)
{
    core_t *core = malloc (sizeof (core_t));
    if (!core)
        return NULL;

    core->appname = strdup (appname);
    core->core_settings = core_settings;

    return core;
}

void free_core (core_t *core)
{
    if (!core)
        return;

    free (core->appname);
    core->appname = NULL;

    free (core);
    core = NULL;
}

int run (core_t *self)
{
    printf("Hi, my name is %s!\n", self->appname);
    return 0;
}
