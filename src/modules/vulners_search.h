#ifndef _VULNERS_SEARCH_
#define _VULNERS_SEARCH_

#include "../core/module.h"

/*
    Search for vulns in vulners db using it's API.
    |args| should consist of version and application's name.
    Returning type is MOD_RES_FIN, or MOD_RES_ERR if error happend.
*/
module_result_t *vulners_search_exec (int argc, const module_data_t argv[]);

#endif
