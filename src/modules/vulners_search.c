#include "vulners_search.h"

#include <stdlib.h>
#include <stdio.h>

module_result_t *vulners_search_exec (int argc, const module_data_t argv[])
{
    printf("GOT DATA: %s\n", (char *) argv[0].value);

    module_data_t *data = create_module_data (MOD_DATA_FIN, "links to vulns");
    if (!data)
        return NULL;

    module_result_t *res = create_module_result (data, NULL);
    if (!res)
        return NULL;

    return res;
}

static module_data_type_t vulners_in_data_types[] = {
    MOD_DATA_APPNAME, MOD_DATA_APPVERSION
};

static module_data_type_t vulners_out_data_types[] = {
    MOD_DATA_FIN, MOD_DATA_ERR
};

module_t vulners_search = {
    MOD_SEARCH,
    "Vulners Search Module",
    2, vulners_in_data_types,
    2, vulners_out_data_types,
    vulners_search_exec
};