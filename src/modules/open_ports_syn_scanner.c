#include "open_ports_syn_scanner.h"

#include <stdlib.h>

module_result_t *open_ports_syn_scanner_exec (int argc,
    const module_data_t argv[])
{
    module_data_t *data = create_module_data (MOD_DATA_PORT, "open ports");
    if (!data)
        return NULL;

    module_result_t *res = create_module_result (data, NULL);
    if (!res)
        return NULL;

    return res;
}

static module_data_type_t open_ports_syn_scanner_in_data_types[] = {
    MOD_DATA_IPADDR
};

static module_data_type_t open_ports_syn_scanner_out_data_types[] = {
    MOD_DATA_PORT, MOD_DATA_ERR
};

module_t open_ports_syn_scanner = {
    MOD_SCAN,
    "Open ports SYN scanner",
    1, open_ports_syn_scanner_in_data_types,
    2, open_ports_syn_scanner_out_data_types,
    open_ports_syn_scanner_exec
};
