#ifndef _OPEN_PORTS_SYN_SCANNER_
#define _OPEN_PORTS_SYN_SCANNER_

#include "../core/module.h"

/*
    Scan for open ports using SYN scanning. Returns MOD_DATA_OPORTS
    if all is ok or MOD_DATA_ERR if error in scanning ports occured.
*/
module_result_t *open_ports_syn_scanner_exec (int argc,
    const module_data_t argv[]);


#endif
