#include <stdio.h>
#include <string.h>
// #includes <stdlib.h>

#include "core/core.h"


int main(int argc, char const *argv[])
{
    // module_data_type_t dt[] = {
    //      MOD_DATA_APPNAME, MOD_DATA_APPVERSION, MOD_DATA_IPADDR
    // };
    // module_t **modules = get_next_modules (3, dt);

    // if (modules) {
    //     printf ("might be working\n");
    //     module_data_t args[0];
    //     args[0].type = MOD_DATA_APPVERSION, 
    //     args[0].value = "data";

    //     module_result_t *res = modules[0]->exec(1, args);

    //     printf ("%s\n", (char *) res->data->value);

    //     free_module_result (res);
    // }
    // else
    //     printf ("bad\n");

    core_t *core = create_core ("LOOL", 0);
    int res = run (core);
    free_core (core);
    return res;
}