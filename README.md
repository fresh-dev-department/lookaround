# Lookaround
## Description
1. Parse ip addresses from stdin/file. Parse router address if present. e.g.:
```lookaround 192.168.1.134```
```lookaround --ips-file=ips.txt --router-ip=192.168.1.1```
2. Get all ips of working PCs/devices using ping(ICMP Echo Request), SYN+FIN ping (send the syn tcp packet and if got syn/ack send the fin packet, useless if victims' open ports are unknown, or only udp ports are available), other way to determine online devices connected to a network.
3. Determine the router's ip address.
4. After that, divide scanning to two different processes: one for a router and another for the devices.
5. Scan router's open ports, analyze web interface if it is present.
6. Scan devices by open ports, using version determining, etc to search for 'em in different CVC/CVV databases. Add links to the log if something is found.
7. Export scanning log (plain text file, console standard output, JSON, XML)
8. Use some colors to bright up the log.
9. Use threads (pthread lib).
10. Some lib's can be used for modules (libpthread, libcurl, libxml2, some json libs)

## Project structure
```
lookaround/
    bin/
        lookaround
    src/
        main.c
        kernel/
        modules/
        network/
        export/
            json/
            xml/
        log/
    data/ (a standard directory for data, can be changed)
        log.txt             \
        exportname.json      > the examples of program's output
        testingexport.xml   /
```